
public class Circle extends Ellipse implements Move {

    public Circle(int r) {
        super(r, r);
    }

    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
