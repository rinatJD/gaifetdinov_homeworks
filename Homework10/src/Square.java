
public class Square extends Rectangle implements Move {

    public Square(int a) {
        super(a, a);
    }

    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
