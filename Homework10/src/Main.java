/*Сделать класс Figure из задания 09 абстрактным.
Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.*/

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        var array = new ArrayList<Move>();
        array.add(new Circle(4));
        array.add(new Square(7));

        for (Move figure: array) {
            figure.move(6, 7);
            System.out.println(figure.toString());
        }
    }
}
