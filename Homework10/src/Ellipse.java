
public class Ellipse extends Figure {

    private int r1;
    private int r2;

    public Ellipse(int r1, int r2) {
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * (r1 + r2);
    }
}
