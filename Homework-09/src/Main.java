/*Сделать класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.*/
public class Main {

    public static void main(String[] args) {

        var figure          = new Figure();
        Ellipse ellipse     = new Ellipse(3,9);
        Circle circle       = new Circle(3);
        Rectangle rectangle = new Rectangle(10,20);
        Square square       = new Square(10);

        System.out.println(figure.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());

    }
}
