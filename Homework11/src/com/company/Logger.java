package com.company;

public class Logger {

    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println(message);
    }

}
