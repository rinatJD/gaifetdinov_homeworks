package com.company;
/**
 * Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
 *
 * Применить паттерн Singleton для Logger.
 */
public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        Logger logger1 = Logger.getInstance();
        logger.log("Что-то");

        System.out.println(logger == logger1);
    }
}
