package com.company;

import java.util.ArrayList;

public class Sequence {
    public Sequence() {
    }

    public static int[] filter(int[] array, ByCondition condition) {
        ArrayList<Integer> newArray = new ArrayList();

        for (int x: array) {
            if (condition.isOk(x)) {
                newArray.add(x);
            }
        }

        int[] a = new int[newArray.size()];

        for(int i = 0; i < newArray.size(); ++i) {
            a[i] = (Integer)newArray.get(i);
        }

        return a;
    }
}