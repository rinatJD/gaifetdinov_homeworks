package com.company;

import java.util.Arrays;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 45, 33, 60, -5, 100, 0, 4, 11, 32};
        int[] a = Sequence.filter(array, (x) -> {
            return x % 2 == 0;
        });
        System.out.println("Чётные элементы: " + Arrays.toString(a));
        int[] b = Sequence.filter(array, (x) -> {
            int digitsSum = 0;

            for(int n = x; n != 0; n /= 10) {
                int lastDigit = n % 10;
                digitsSum += lastDigit;
            }

            return digitsSum % 2 == 0;
        });
        System.out.println("Сумма цифр является четным числом: " + Arrays.toString(b));
    }
}
