/*На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).

Считать эти данные в массив объектов.

Вывести в отсортированном по возрастанию веса порядке.*/

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //добавим по 5 человек разными конструкторами
        var human_01 = new Human();
        human_01.setName("Энтони Джеймс Марстон");
        human_01.setWeight(65.6);
        var human_02 = new Human();
        human_02.setName("Этель Роджерс");
        human_02.setWeight(56.6);
        var human_03 = new Human();
        human_03.setName("Джон Гордон Макартур");
        human_03.setWeight(77.7);
        var human_04 = new Human();
        human_04.setName("Томас Роджерс");
        human_04.setWeight(85.6);
        var human_05 = new Human();
        human_05.setName("Эмили Каролина Брент");
        human_05.setWeight(95.6);
        var human_06 = new Human("Лоуренс Джон Уоргрейв", 45);
        var human_07 = new Human("Эдуард Джордж Армстронг", 75.2);
        var human_08 = new Human("Уильям Генри Блор", 95.23);
        var human_09 = new Human("Филипп Ломбард", 75.1);
        var human_10 = new Human("Вера Элизабет Клейторн ", 45d);
        // запишем эти данные в массив
        var array = new ArrayList<Human>();
        array.add(human_01);
        array.add(human_02);
        array.add(human_03);
        array.add(human_04);
        array.add(human_05);
        array.add(human_06);
        array.add(human_07);
        array.add(human_08);
        array.add(human_09);
        array.add(human_10);
        //отсортируем массив по возрастанию веса с использованием лямбда-выражения
        array.sort((x1, x2) -> Double.compare(x1.getWeight(), x2.getWeight()));
        //выведем в консоль
        System.out.println(array.toString());

    }
}
