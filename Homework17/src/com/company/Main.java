package com.company;

import java.util.HashMap;
import java.util.Map;
/*На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
Вывести:
Слово - количество раз
Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
*/
public class Main {

    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();


        String text =   "Вы помните, помните, " +
                        "Вы всё, конечно, помните, " +
                        "Как я стоял, " +
                        "Приблизившись к стене, " +
                        "Взволнованно ходили Вы по комнате " +
                        "И что-то резкое " +
                        "В лицо бросали мне.";

        String[] arrayText = text.split(" ");

        for (String textFromArray : arrayText) {
            map.computeIfPresent(textFromArray, (key, value) -> ++value);
            map.computeIfAbsent(textFromArray, (value -> 1));
        }

        map.forEach((key, value) -> {
            System.out.println("Слово \"" + key + "\" встречается " + value + " раз(а).");
        });
   }

}
