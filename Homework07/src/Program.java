import java.util.*;
/**
 * На вход подается последовательность чисел, оканчивающихся на -1.
 * Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
 * Гарантируется:
 * Все числа в диапазоне от -100 до 100.
 * Числа встречаются не более 2 147 483 647-раз каждое.
 * Сложность алгоритма - O(n)
 */
public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        byte a = scanner.nextByte();
        int[] array = new int[201];
        int min = Integer.MAX_VALUE, minIndex = -1;
        // если в консоль -1, то выходим сразу, будем считать, что это число не учавствует в последовательности,
        // т.к. в противном случае получаем элемент, который гарантированно входит в последовательность минимальное количество раз (один раз)
        while (a != -1) {
            int num = a + 100;
            array[num]++;
           a = scanner.nextByte();
        }
        for (int i = 0; i < array.length; i++) {
            if (min > array[i] && array[i] != 0) {
                min = array[i];
                minIndex = i - 100;
            }
        }

//        System.out.println(Arrays.toString(array));
//        System.out.println(min);
        System.out.println("Минимальное количество раз встречается число: " + minIndex);

    }
}

