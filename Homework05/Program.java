import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt(), minDigit = Integer.MAX_VALUE;
        while (a != -1) {
            while (a != 0) {
                int lastDigit = a % 10;
                minDigit = Math.min(lastDigit, minDigit);
                a = a / 10;
            }
            a = scanner.nextInt();
        }
        System.out.println(minDigit);
    }
}
