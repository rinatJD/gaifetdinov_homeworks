package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        ArrayList<Integer> newArray = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(i)) {
                newArray.add(array[i]);
            }
        }

        int[] a = new int[newArray.size()];
        for (int i = 0; i < newArray.size(); i++) {
            a[i] = newArray.get(i);
        }

        return a;
    }

}
