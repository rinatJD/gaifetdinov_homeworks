package com.company;
/**
 * Предусмотреть функциональный интерфейс
 *
 * interface ByCondition {
 * 	boolean isOk(int number);
 * }
 *
 * Реализовать в классе Sequence метод:
 *
 * public static int[] filter(int[] array, ByCondition condition) {
 * 	...
 * }
 *
 * Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
 *
 * В main в качестве condition подставить:
 *
 * - проверку на четность элемента
 * - проверку, является ли сумма цифр элемента четным числом.
 */

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] array = {1, 2, 45, 33, 60, -5, 100, 0, 4};
        //проверка на четность элемента
        int[] a = Sequence.filter(array, x -> array[x] % 2 == 0);
        System.out.println("Чётные элементы: " + Arrays.toString(a));

        //проверка, является ли сумма цифр элемента четным числом.
        int[] b = Sequence.filter(array, x -> {
            int digitsSum = 0;
            int n = array[x];
            while (n != 0) {
                int lastDigit = n % 10;
                digitsSum = digitsSum + lastDigit;
                n = n / 10;
            }
            return digitsSum % 2 == 0;
        });
        System.out.println("Сумма цифр является четным числом: " + Arrays.toString(b));

    }
}
