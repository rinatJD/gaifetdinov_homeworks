/*
1) Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.

2) Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20

стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
*/


import java.util.ArrayList;
import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        int[] array = {234, 0, 0, 435, 0, 324, 213, 54 , 0, 34, 0};
        int digit = 435;
        System.out.println("Индекс искомого числа в массиве: " + searchDigitInArray(array, digit));
        System.out.println(Arrays.toString(moveDigitLeft(array)));
    }

    public static int searchDigitInArray(int[] array, int digit) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == digit) {
                return i;
            }
        }
        return -1;
    }

    public static int[] moveDigitLeft(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] != 0) {
                        array[i] = array[j];
                        array[j] = 0;
                        break;
                    }
                }
            }
        }
        return array;
    }
}
